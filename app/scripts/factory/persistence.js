app.factory("persistence", [
	"$cookies", function($cookies) {
		
		var id = 0;
		var name = "";
		var email = "";

		return {
			setUser: function(id,name,email) {
				name = name;
				email = email;
				id = id;

				$cookies.put("id", id);
				$cookies.put("name", name);
				$cookies.put("email", email);
			},
			getUser: function() {
				id = $cookies.get("id");
				name = $cookies.get("name");
				email = $cookies.get("email");
				return { 'name': name, 'email' : email, 'id': id };
			},
			clearUser: function() {
				id = "";
				name = "";
				email = "";
				
				$cookies.remove("id");
				$cookies.remove("name");
				$cookies.remove("email");
			}
		}
	}
]);