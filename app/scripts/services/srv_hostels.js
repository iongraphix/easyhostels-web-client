app.service('srv_hostels', function ($http){
    var host = $("body").data("url");
    
    this.create = function(data){
    		var template_url = "http://"+host+"/resource/hostel";
    		return $http.post(template_url, data);
    }
    this.edit = function(data){
    	var template_url = "http://"+host+"/resource/hostel";
    		template_url += "/"+data.id;
            return $http.put(template_url, data);
    }
    this.getAll = function(){
    	var template_url = "http://"+host+"/resource/hostel";
    	return $http.get(template_url);
    }
    this.setPrimary = function(data){
        var template_url = "http://"+host+"/resource/hostel/upload/owner";
        return $http.post(template_url, data);
    }

    this.getOwnHostels = function(id){
        var template_url = "http://"+host+"/resource/hostel/owner";
        template_url += "/"+id;
        console.log("Shipping a new url: "+template_url);
        return $http.get(template_url);
    }

    this.delete = function(id){
    	var template_url = "http://"+host+"/resource/hostel";
    	template_url += "/"+id;
    	return $http.delete(template_url);
    }

    this.getSingle = function(data){
	    var template_url = "http://"+host+"/resource/hostel";
		template_url +="/"+data.id;
        console.log("Trying to nigger: "+template_url);
		return $http.get(template_url);
    }
});