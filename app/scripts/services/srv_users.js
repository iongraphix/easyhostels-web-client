app.service('srv_users', function ($http){
    var host = $("body").data("url");
    
    this.createUser = function(data){
    		var template_url = "http://"+host+"/resource/user";
    		return $http.post(template_url, data);
    }
    this.uploadImage = function(data){
            var template_url = "http://"+host+"/upload/user";
            return $http.post(template_url, data);
    }
    this.editUser = function(data){
    	var template_url = "http://"+host+"/resource/user";
    		template_url += "/"+data.id;
    		console.log("Formulated Query Just Now : "+template_url);
    		return $http.put(template_url, data);
    }
    this.getAll = function(){
    	var template_url = "http://"+host+"/resource/user";
    		return $http.get(template_url);
    }
    this.deleteUser = function(id){
    	var template_url = "http://"+host+"/resource/user";
    	template_url += "/"+id;
    	return $http.delete(template_url);
    }
    this.getSingle = function(data){
    	var template_url = "http://"+host+"/resource/user";
    		template_url +="/"+data.id;
    		console.log("Formulated Query Just Now : "+template_url);
    		return $http.get(template_url);
    }
});