app.controller('BrowseController', function($scope,$cookies,$mdToast,$mdDialog,$state,$rootScope,$localStorage,persistence,srv_users, srv_hostels) {
  
  
  $scope.loadItems = function(){
    srv_hostels.getOwnHostels($rootScope.user.id).then(function(res){
      console.log("Baby Crying with data");
      console.dir(res);
      var data = res.data;
    console.dir(data);
    $scope.hostels = data;

  },function(res){
    console.dir(res);
    $("body").html(res.data);
  });

  };

  if($rootScope.refresh == true){
    $state.reload();
    console.log("Yeah Man its on");
    $rootScope.refresh = false;
  };
  var host = $("body").data("url");
  $scope.mother = host;
  $scope.loadItems(); // Calling load items

  
  if($cookies.get("logged_in") === undefined){
        $state.go("access.signin");
        $mdToast.showSimple('Not so fast ;)');
  }

  $scope.details  = persistence.getUser();
  $scope.$watch('details', function(newValue, oldValue){
       $rootScope.user = newValue;
    }, true);

  srv_users.getSingle($scope.details).then(function(res){
    console.log("Got Sinle");
    var data = res.data;
    $scope.details = data;
  },function(res){

  });








  $scope.test = function(){
      alert("Testing Mic");
  }
  $scope.edit = function(hostel){
      $cookies.put('edit_hostel', JSON.stringify(hostel));
      $state.go('hostels.edit');     
  };
  
  $scope.remove = function(hostel){
      $scope.showConfirm(hostel);
  };

  $scope.showConfirm = function(hostel) {
      console.dir(hostel);
      var confirm = $mdDialog.confirm()
        .title('Would you like to delete '+hostel.display_name+'?')
        .content('All of the related entities to this user will also be deleted.')
        .ariaLabel('Lucky day')
        .ok('Yes, please!')
        .cancel('NO, I didn\'t mean to.');

      $mdDialog.show(confirm).then(function() {
          // if yes
          srv_hostels.delete(hostel.id).then(function(res){
            var data = res.data;
            if(data.status == "success"){
              $mdToast.showSimple('Hostel has successfully been removed');            
              $scope.loadItems();
            }
      }, function(res) {
          // if no
          console.log("Error");
          console.dir(res);
          $("body").html(res.data);
      })
    });

  };

  $scope.show = function(hostel){
    $cookies.put('show_hostel', JSON.stringify(hostel));
    $state.go('hostels.view');
  }
});