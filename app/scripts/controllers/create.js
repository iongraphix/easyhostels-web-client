app.controller('CreateController', function($scope,$cookies,$mdToast,$mdDialog,$state,$rootScope,$localStorage,persistence,srv_users, srv_hostels) {
   
    $scope.form = true;
    $scope.image = false;
    $scope.create = true;
    $scope.hostel = {};
    $scope.hostel.lat = 0.0;
    $scope.hostel.lng = 0.0;
    $scope.hostel.available = true;
    $scope.hostel.user_id = $rootScope.user.id;
    $scope.hostel.notes = "Default Notes";

   $rootScope.$watch('curr_hostel', function(newValue, oldValue){
       console.log("Its Changing");
       if($rootScope.curr_hostel === undefined){

       }else {
        var lat = $rootScope.curr_hostel.lat;
        var lng = $rootScope.curr_hostel.lng;
        $scope.hostel.lat = lat;
        $scope.hostel.lng = lng;
       }
    }, true);

   $scope.sendImage = function(){
      var data  = {
        "hostel_id" : $scope.hostel_id,
        "image": $scope.myCroppedImage
      };
      console.log("Payload");
      console.dir(data);
      srv_hostels.setPrimary(data).then(function(res){
         $state.go('hostels.browse');
      }, function(res){
          $("body").html(res.data);
      });
   }

     $scope.shipIt = function(){
        console.log($scope.hostel);
        srv_hostels.create($scope.hostel).then(function(res){
            var data = res.data;
            var id = data.info.id;

            $scope.hostel_id = id;
            console.dir(data);
            if(data.status == "success"){
                $scope.form = false;
                $scope.image = true;
                $scope.myImage = '';
                $scope.myCroppedImage='';
                $scope.cropType="square";
                $mdToast.showSimple('Hostel Created');
            }

        },function(res){
            console.log("Got Back");
            $("body").html(res.data);
            console.dir(res);
        });
     };

    if($cookies.get("logged_in") === undefined){
        $state.go("access.signin");
        $mdToast.showSimple('Not so fast ;)');
   	}

    $scope.details  = persistence.getUser();
    $scope.$watch('details', function(newValue, oldValue){
         $rootScope.user = newValue;
      }, true);

    srv_users.getSingle($scope.details).then(function(res){
    	console.log("Got Sinle");
    	var data = res.data;
    	$scope.details = data;
    },function(res){

    });

     $scope.showMapDialog = function(ev) {
		    var vscope = $scope;
        $mdDialog.show({
		      controller: MapDialogController,
		      templateUrl: 'views/pages/tmpl/google-map.tmpl.html',
		      targetEvent: ev,
		    })
		    .then(function(answer) {
		    	// Code Here
          console.log("I got it: "+$rootScope.curr_hostel);
          var lat = $rootScope.curr_hostel.lat;
          var lng = $rootScope.curr_hostel.lng;
		    }, function() {

		    });
  	};

  	var handleFileSelect=function(evt) {
      var file=evt.currentTarget.files[0];
      var reader = new FileReader();
      reader.onload = function (evt) {
        $scope.$apply(function($scope){
          $rootScope.myImage=evt.target.result;
          $scope.myImage = evt.target.result;
        });
      };

      if(document.getElementById("fileInput").files.length != 0){
      	reader.readAsDataURL(file);
      }
    };

    angular.element(document.querySelector('#fileInput')).on('change',handleFileSelect);

});

function MapDialogController($scope, $mdDialog,$rootScope,$mdToast) {
  console.log("Loaded Map Ctrl");
  $scope.hide = function() {
    $mdDialog.hide();
  };

  $scope.cancel = function() {
    $mdDialog.hide();
  };
  $scope.answer = function(answer) {
      $mdDialog.hide(answer);	
  };
};