app.controller('ViewController', function($scope,$cookies,$mdToast,$mdDialog,$state,$rootScope,$localStorage,persistence,srv_users, srv_hostels) {
   
    $scope.hostel = JSON.parse($cookies.get('show_hostel'));
    var host = $("body").data("url");
    $scope.mother = host;
    $scope.hostel_profile_img = "http://"+$scope.mother+"/images/hostel/"+$scope.hostel.id+".jpg";

    console.log("hostel ID: "+$scope.hostel.id);

    srv_hostels.getSingle($scope.hostel).then(function(res){
        var data = res.data;
        console.log("I got the information");
        $scope.hostel = data;
        console.dir(res);
    },function(res){
      $("body").html(res.data);
    });

    $scope.edit = function(){
        $cookies.put('edit_hostel', JSON.stringify($scope.hostel));
        $state.go('hostels.edit');     
    };


});