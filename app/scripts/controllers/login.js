app.controller('LoginController', function($scope,$cookies,$mdToast,$state,$rootScope,auth,$localStorage,persistence) {
    
    $rootScope.user.name = "";
    $rootScope.user.email = "";
    $scope.user.password = "";

    $scope.showPassword = function(e){
    	var email = $scope.user.email;
    	var password = $scope.user.password;

            auth.tryAuth(email,password).then(function(res){
                
                var data = res.data;
                if(data.status == "success"){
                    var role = data.info.role.title;
                    
                    $rootScope.user.name = data.info.name;
                    $rootScope.user.email = data.info.email;

                    if(role == "Student"){
                        $mdToast.showSimple('Hello '+data.info.name+'. Please Download the App and Login');
                    }
                    else if(role == "Owner") {
                        $cookies.put("told", false);
                        $cookies.put("logged_in", "yes");
                        persistence.setUser(data.info.id, data.info.name, data.info.email);
                        $state.go('hostels.dashboard');
                    }
                }
                else if(data.status == "error"){
                    $mdToast.showSimple('Sorry, those credentials don\'t work.');
                }
            }, function(res){
                console.log("An Error Occurred!");
                $("body").html(res.data);
                $mdToast.showSimple('An Error Occurred, Check your internet connection');
                console.dir(res);
            });    
        
    };
});