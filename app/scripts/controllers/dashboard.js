app.controller('DashboardController', function($scope,$cookies,$mdToast,$state,$rootScope,$localStorage,srv_users,persistence) {
    if($cookies.get("logged_in") === undefined){
        $state.go("access.signin");
        $mdToast.showSimple('Not so fast ;)');
    }

    
    console.log("Displaying cookies");
    console.dir($cookies.get("logged_in"));

    if($rootScope.newuser == true){
        $mdToast.showSimple('Hi '+$rootScope.name+', Welcome to EasyHostels Owners Portal');
        $rootScope.newuser = false;  
    }

    $scope.details  = persistence.getUser();
    $scope.$watch('details', function(newValue, oldValue){
         $rootScope.user = newValue;
      }, true);

    srv_users.getSingle($scope.details).then(function(res){
        console.log("Got Sinle");
        var data = res.data;
        $scope.details = data;
    },function(res){

    });


});