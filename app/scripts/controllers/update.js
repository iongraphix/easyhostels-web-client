app.controller('EditController', function($scope,$cookies,$mdToast,$mdDialog,$state,$rootScope,$localStorage,$window,persistence,srv_users,srv_hostels) {
    
    $scope.hostel = JSON.parse($cookies.get('edit_hostel'));
    var host = $("body").data("url");
    $scope.mother = host;
    $scope.hostel_profile_img = "http://"+$scope.mother+"/images/hostel/"+$scope.hostel.id+".jpg";


    $scope.form = true;
    $scope.image = false;
    $scope.create = true;
    $scope.myEditImage = '';
    $scope.myCroppedImage='';
    $scope.myCroppedImage='';
  

    $scope.showImage = function(){
        $scope.form = false;
        $scope.image = true;
        $scope.cropType='square';
    };
   $scope.back = function(){
        $scope.form = true;
        $scope.image = false;
   }
   $rootScope.$watch('curr_hostel', function(newValue, oldValue){
       console.log("Its Changing");
       if($rootScope.curr_hostel === undefined){

       }else {

        var lat = $rootScope.curr_hostel.lat;
        var lng = $rootScope.curr_hostel.lng;
        $scope.hostel.lat = lat;
        $scope.hostel.lng = lng;
        console.log("I belive I have set them to the new. Lat: "+$scope.hostel.lat+" and LNG: "+$scope.hostel.lng);

       }
    }, true);

   $scope.sendImage = function(){
      var data  = {
        "hostel_id" : $scope.hostel.id,
        "image": $scope.myCroppedImage
      };
      console.log("Payload");
      console.dir(data);
      srv_hostels.setPrimary(data).then(function(res){
         $scope.back();
          $mdToast.showSimple('Image Updated Successfully');
          $scope.hostel_profile_img = $scope.myCroppedImage;
          $rootScope.refresh = true;
      }, function(res){
          $("body").html(res.data);
      });
   }

     $scope.shipIt = function(){
        console.log($scope.hostel);
        srv_hostels.edit($scope.hostel).then(function(res){
            var data = res.data;
            console.dir(data);
            if(data.status == "success"){
                $scope.myEditImage = '';
                $scope.myCroppedImage='';
                $scope.cropType="square";
                $mdToast.showSimple('Hostel Updated Successfully');
                $window.history.back();
            }
        },function(res){
            console.log("Got Back");
            $("body").html(res.data);
            console.dir(res);
        });
     };

    if($cookies.get("logged_in") === undefined){
        $state.go("access.signin");
        $mdToast.showSimple('Not so fast ;)');
   	}

    $scope.details  = persistence.getUser();
    $scope.$watch('details', function(newValue, oldValue){
         $rootScope.user = newValue;
      }, true);

    srv_users.getSingle($scope.details).then(function(res){
    	console.log("Got Sinle");
    	var data = res.data;
    	$scope.details = data;
    },function(res){

    });

     $scope.showMapDialog = function(ev) {
		    var vscope = $scope;
        $mdDialog.show({
		      controller: MapDialogController,
		      templateUrl: 'views/pages/tmpl/google-map.tmpl.html',
		      targetEvent: ev,
		    })
		    .then(function(answer) {
		    	// Code Here
          console.log("I got it: "+$rootScope.curr_hostel);
          var lat = $rootScope.curr_hostel.lat;
          var lng = $rootScope.curr_hostel.lng;
		    }, function() {

		    });
  	};

  	var handleFileSelect=function(evt) {
      var file=evt.currentTarget.files[0];
      var reader = new FileReader();
      reader.onload = function (evt) {
        $scope.$apply(function($scope){
          $rootScope.myEditImage=evt.target.result;
          $scope.myEditImage = evt.target.result;
        });
      };

      if(document.getElementById("fileInput").files.length != 0){
      	reader.readAsDataURL(file);
      }
    };

    angular.element(document.querySelector('#fileInput')).on('change',handleFileSelect);

});
function getBase64FromImageUrl(url) {
    alert(url);
    var img = new Image();

   /* img.setAttribute('crossOrigin', 'anonymous');*/

    img.onload = function () {
        var canvas = document.createElement("canvas");
        canvas.width =this.width;
        canvas.height =this.height;

        var ctx = canvas.getContext("2d");
        ctx.drawImage(this, 0, 0);

        var dataURL = canvas.toDataURL("image/jpg");

        alert(dataURL.replace(/^data:image\/(png|jpg);base64,/, ""));

    };

    img.src = url;
    alert(url);
}


function MapDialogController($scope, $mdDialog,$rootScope,$mdToast) {
  console.log("Loaded Map Ctrl");
  $scope.hide = function() {
    $mdDialog.hide();
  };

  $scope.cancel = function() {
    $mdDialog.hide();
  };
  $scope.answer = function(answer) {
      $mdDialog.hide(answer);	
  };
};