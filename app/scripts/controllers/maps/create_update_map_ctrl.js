
app.controller('CreateUpdateGoogleDiaglogMapCtrl', ['$scope', '$rootScope', '$cookies', function ($scope,$rootScope,$cookies) {
    console.log("Maps Online");
    $scope.myMarkers = [];

    $scope.mapOptions = {
      center: new google.maps.LatLng(5.796575, -0.127094),
      zoom: 9,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    $scope.addMarker = function ($event, $params) {
      
      angular.forEach($scope.myMarkers, function(marker) {
        marker.setMap(null);
      });

      $scope.myMarkers.push(new google.maps.Marker({
        map: $scope.myMap,
        position: $params[0].latLng
      }));

      $rootScope.curr_hostel=$params[0].latLng; 
      console.log("Nice: "+$params[0].latLng);
      consolete.dir($rootScope.curr_hostel);
    };

    $scope.setZoomMessage = function (zoom) {
      $scope.zoomMessage = 'You just zoomed to ' + zoom + '!';
    };


    $scope.setMarkerPosition = function (marker, lat, lng) {
      marker.setPosition(new google.maps.LatLng(lat, lng));
    };
}]);