'use strict';

/**
 * @ngdoc function
 * @name app.config:uiRouter
 * @description
 * # Config
 * Config for the router
 */
angular.module('app')
  .run(
    [           '$rootScope', '$state', '$stateParams',
      function ( $rootScope,   $state,   $stateParams ) {
        $rootScope.$state = $state;
        $rootScope.$stateParams = $stateParams;
      }
    ]
  )
  .config(
    [          '$stateProvider', '$urlRouterProvider', 'MODULE_CONFIG','CONST_APP_NAME',
      function ( $stateProvider,   $urlRouterProvider,  MODULE_CONFIG, CONST_APP_NAME ) {
        $urlRouterProvider
          .otherwise('/hostels/dashboard');
        $stateProvider
          
            // Hostels States
            .state('hostels', {
              url: '/hostels',
              views: {
                '': {
                  templateUrl: 'views/layout.html'
                },
                'aside': {
                  templateUrl: 'views/aside.html'
                },
                'content': {
                  templateUrl: 'views/content.html'
                }
              }
            })
             .state('hostels.dashboard', {
              url: '/dashboard',
              controller: 'DashboardController',
              templateUrl: 'views/pages/home/dashboard.html',
              data : { title: 'Dashboard', folded: false },
              resolve: load(['scripts/services/srv_users.js','scripts/controllers/dashboard.js'])
            })
            .state('hostels.create', {
              url: '/create',
              templateUrl: 'views/pages/hostels/create.html',
              controller: 'CreateController',
              data : { title: 'New Hostel', child: true},
              resolve: load(['ui.map','ngImgCrop','scripts/controllers/create.js','scripts/controllers/maps/create_map_ctrl.js','scripts/controllers/load-google-maps.js','scripts/services/srv_users.js', 'scripts/services/srv_hostels.js'],  function(){ return loadGoogleMaps(); }) 
            })
            .state('hostels.edit', {
              url: '/edit',
              templateUrl: 'views/pages/hostels/update.html',
              controller: 'EditController',
              data : { title: 'Editing Hostel', child: true},
              resolve: load(['ui.map','ngImgCrop','scripts/controllers/update.js','scripts/controllers/maps/create_map_ctrl.js','scripts/controllers/load-google-maps.js','scripts/services/srv_users.js', 'scripts/services/srv_hostels.js'],  function(){ return loadGoogleMaps(); }) 
            })
            .state('hostels.browse', {
              url: '/browse', 
              controller: 'BrowseController',
              templateUrl: 'views/pages/hostels/browse.html',
              data : { title: 'Browse My Hostels' },
              resolve: load(['ui.map','ngImgCrop','scripts/controllers/browse.js','scripts/directives/errSrc.js','scripts/services/srv_users.js', 'scripts/services/srv_hostels.js'])
            })
            .state('hostels.view', {
              url: '/view', 
              controller: 'ViewController',
              templateUrl: 'views/pages/hostels/view.html',
              data : { title: 'Showing Hostel', child: true },
              resolve: load(['ui.map','ngImgCrop','scripts/controllers/view.js','scripts/services/srv_users.js', 'scripts/services/srv_hostels.js'])
            })
            .state('hostels.reviews', {
              url: '/reviews',
              templateUrl: 'views/pages/hostels/reviews.html',
              data : { title: 'Hostel Reviews' }
            })

            .state('hostels.profile', {
              url: '/profile',
              controller: 'ProfileController',
              templateUrl: 'views/pages/home/profile.html',
              data : { title: 'User Profile', child: true },
              resolve: load(['ngImgCrop','scripts/services/srv_users.js','scripts/controllers/profile.js'])
             })


            // Status States
            .state('404', {
              url: '/404',
              templateUrl: 'views/pages/404.html'
            })
            .state('505', {
              url: '/505',
              templateUrl: 'views/pages/505.html'
            })

            // Authentitication States
            .state('access', {
              url: '/access',
              template: '<div class="deep-purple bg-big"><div ui-view class="fade-in-down smooth"></div></div>'
            })
           .state('access.signin', {
              url: '/signin',
              controller: 'LoginController',
              templateUrl: 'views/pages/auth/signin.html',
              resolve: load(['scripts/services/login.js','scripts/controllers/login.js'])
            })
            .state('access.signup', {
              url: '/signup',
              controller: 'SignupController',
              templateUrl: 'views/pages/auth/signup.html',
              resolve: load(['scripts/services/signup.js', 'scripts/controllers/signup.js'])
            })
            .state('access.forgot-password', {
              url: '/forgot-password',
              templateUrl: 'views/pages/auth/forgot-password.html'
            })
            .state('access.lockme', {
              url: '/lockme',
              templateUrl: 'views/pages/auth/lockme.html'
            });


          function load(srcs, callback) {
            return {
                deps: ['$ocLazyLoad', '$q',
                  function( $ocLazyLoad, $q ){
                    var deferred = $q.defer();
                    var promise  = false;
                    srcs = angular.isArray(srcs) ? srcs : srcs.split(/\s+/);
                    if(!promise){
                      promise = deferred.promise;
                    }
                    angular.forEach(srcs, function(src) {
                      promise = promise.then( function(){
                        angular.forEach(MODULE_CONFIG, function(module) {
                          if( module.name == src){
                            if(!module.module){
                              name = module.files;
                            }else{
                              name = module.name;
                            }
                          }else{
                            name = src;
                          }
                        });
                        return $ocLazyLoad.load(name);
                      } );
                    });
                    deferred.resolve();
                    return callback ? promise.then(function(){ return callback(); }) : promise;
                }]
            }
          }
      }
    ]
  );
